﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace MathTests.Models
{
    internal class Review
    {
        private string _comment;
        private int _rating;
        public string Comment
        {
            get => _comment;
            set
            {
                if (value.Length > 50)
                {
                    Log.Info(Constants.AppName, "Comment is too long");
                }
                else
                {
                    _comment = value;
                }
            }
        }

        public int Rating
        {
            get => _rating;
            set
            {
                if (value < 1 || value > 5)
                {
                    Log.Error(Constants.AppName, "Rating must be between 1 and 5");
                }
                else
                {
                    _rating = value;
                }
            }
        }
    }
}