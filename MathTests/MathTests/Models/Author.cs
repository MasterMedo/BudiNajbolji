﻿using System.Collections.Generic;
using Android.Util;
using Firebase.Xamarin.Database;
using Firebase.Xamarin.Database.Query;

namespace MathTests.Models
{
    internal class Author : RegUser
    {
        public Author(string name, string lastName, string contactNumber, string username, string email, string school)
            : base(name, lastName, contactNumber, username, email)
        {
            School = school;
        }

        public string School { get; }
        public List<TaskTemplate> MyTasks { get; set; }
        public List<TestTemplate> MyTests { get; set; }

        public async void AddTask(TaskTemplate task)
        {
            if (!MyTasks.Contains(task))
            {
                var firebase = new FirebaseClient(Constants.FirebaseURL);
                MyTasks.Add(task);
                var item = await firebase.Child("Authors").PostAsync(this);
            }
            else
            {
                Log.Info(Constants.AppName, "Task already exists");
            }
        }

        public void AddTest(TestTemplate test)
        {
            if (!MyTests.Contains(test))
                MyTests.Add(test);
            else
                Log.Info(Constants.AppName, "Test already exists");
        }

        public async void DeleteTask(TaskTemplate task)
        {
            if (MyTasks.Contains(task))
            {
                var firebase = new FirebaseClient(Constants.FirebaseURL);
                MyTasks.Remove(task);
                await firebase.Child("Authors").Child("UserId").PostAsync(this);
            }
            else
            {
                Log.Info(Constants.AppName, "Task doesn't exist");
            }
        }

        public void DeleteTest(TestTemplate test)
        {
            if (MyTests.Contains(test))
                MyTests.Remove(test);
            else
                Log.Info(Constants.AppName, "Test doesn't exist");
        }

        public void UpdateTask(TaskTemplate task)
        {
            if (MyTasks.Contains(task))
            {
                MyTasks.Remove(task);
                MyTasks.Add(task);
            }
            else
            {
                Log.Info(Constants.AppName, "Task doesn't exist");
            }
        }

        public void UpdateTest(TestTemplate test)
        {
            if (MyTests.Contains(test))
            {
                MyTests.Remove(test);
                MyTests.Add(test);
            }
            else
            {
                Log.Info(Constants.AppName, "Test doesn't exist");
            }
        }

        public void ReviewTask(TaskTemplate task, Review review)
        {
        }

        public void ReviewRandomTask()
        {
        }

        public void ReviewTest(TestTemplate  test)
        {
        }

        public void ReviewRandomTest()
        {
        }
    }
}