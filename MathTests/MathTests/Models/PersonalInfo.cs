﻿namespace MathTests.Models
{
    internal class PersonalInfo
    {
        public PersonalInfo(string name, string lastName, string contactNumber, string username, string email)
        {
            Name = name;
            LastName = lastName;
            ContactNumber = contactNumber;
            Username = username;
            Email = email;
        }

        public string Name { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}