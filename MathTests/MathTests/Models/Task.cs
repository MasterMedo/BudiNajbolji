﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MathTests.Models
{
    internal class Task
    {
        public Task() { TaskId = Guid.NewGuid(); }
        public Guid TaskId { get; }
        //public List<Object> TaskText;
        //public Solution Solution;




        protected bool Equals(Task other)
        {
            return TaskId.Equals(other.TaskId);
        }

        public override bool Equals(Object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Task) obj);
        }
        public override int GetHashCode()
        {
            return TaskId.GetHashCode();
        }
    }
}