﻿using System;
using Android.Util;
using Android.Widget;

namespace MathTests.Models
{
    internal abstract class RegUser : User
    {
        private string _username;

        public string Username
        {
            get => _username;
            set
            {
                if (value.Length < 8 || value.Length > 16)
                {
                    Log.Info(Constants.AppName, "Username must be between 8 and 16 characters long");
                }
                else
                {
                    _username = value;
                }
            }
        }
        public PersonalInfo PersonalInfo;
        // public string ProfileImagePath;

        protected RegUser(string name, string lastName, string contactNumber, string username, string email)
        {
            PersonalInfo = new PersonalInfo(name, lastName, contactNumber, username, email);
            Username = username;
        }

        public void UpdatePersonalInfo(string name, string lastName, string contactNumber, string username, string email)
        {
            if (name != null) PersonalInfo.Name = name;
            if (lastName != null) PersonalInfo.LastName = lastName;
            if (contactNumber != null) PersonalInfo.ContactNumber = contactNumber;
            if (username != null) PersonalInfo.Username = username;
            if (email != null) PersonalInfo.Email = email;
        }
    }
}