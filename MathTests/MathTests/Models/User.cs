﻿using System;
using System.Collections.Generic;

namespace MathTests.Models
{
    internal abstract class User
    {
        protected User() { UserId = Guid.NewGuid(); }
        public Guid UserId { get; }

        // All other views need to be implemented here as well (return type is probably "View")
        public void ViewScoreboardPage()
        {
            // Get all users
        }

        public void ViewTests(List<string> filters)
        {
            // Get all tests
        }

        void ViewTasks(List<string> filters)
        {
            // Get all task
        }

        public void ViewMainPage() { }
    }
}