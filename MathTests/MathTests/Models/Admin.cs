﻿namespace MathTests.Models
{
    internal class Admin : RegUser
    {
        public Admin(string name, string lastName, string contactNumber, string username, string email) : base(name,
            lastName, contactNumber, username, email)
        {

        }

        public void DeleteUser(User user)
        {

        }
        public void DeleteTask() { }
        public void DeleteTest() { }

    }
}