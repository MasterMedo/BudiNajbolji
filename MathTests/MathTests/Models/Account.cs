﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MathTests.Models
{
    internal class Account
    {
        public Guid UserId { get; } = Guid.NewGuid();
        public string Name { get; set; }
        public string Password { get; set; }
    }
}