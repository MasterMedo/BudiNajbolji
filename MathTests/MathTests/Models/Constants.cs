﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Firebase.Xamarin.Database;

namespace MathTests.Models
{
    internal static class Constants
    {
        public const string FirebaseURL = "https://mathtests-fer.firebaseio.com/";

        public const string AppName = "MathTests";
    }
}