﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MathTests.Models
{
    internal class Test
    {
        public Guid TestId { get; } = Guid.NewGuid();

        protected bool Equals(Test other)
        {
            return TestId.Equals(other.TestId);
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Test) obj);
        }

        public override int GetHashCode()
        {
            return TestId.GetHashCode();
        }
    }
}