﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Util;
using Firebase.Xamarin.Database;
using Firebase.Xamarin.Database.Query;
using MathTests.Models;

namespace MathTests
{
    [Activity(Label = "@string/app_name", MainLauncher = true, LaunchMode = Android.Content.PM.LaunchMode.SingleTop, Icon = "@drawable/icon")]
    public class MainActivity : AppCompatActivity
    {
        private CoordinatorLayout rootLayout;
        private Button btnShowSnackbar;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            // CreateAccount();
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.main);
            rootLayout = FindViewById<CoordinatorLayout>(Resource.Id.root_view);
            btnShowSnackbar = FindViewById<Button>(Resource.Id.btn_show_snackbar);
            btnShowSnackbar.Click += delegate
            {
                Snackbar snackbar = Snackbar.Make(rootLayout, "This is a snackbar!", Snackbar.LengthShort).SetAction(
                    "OK",
                    (view) => { Toast.MakeText(this, "Snackbar Clicked", ToastLength.Short).Show(); });
                View snackbarView = snackbar.View;
                snackbar.Show();
            };
        }

        protected async void CreateAccount()
        {
            var firebase = new FirebaseClient(Constants.FirebaseURL);
            await firebase.Child("Admins").PostAsync(new Admin("admin", "adminovic", "neki broj", "123456778", "admin@admin.com"));

            var ivica = new Author("Ivan", "Mestar", "kontaktni broj", "jusernejm", "imejl", "mijoc");
            var perica = new Author("Pero", "Mestar", "09898989898", "Ivica1235456", "Ivica@gmail.com", "mioc");

            var item1 = await firebase.Child("Authors").PostAsync(ivica);
            var item2 = await firebase.Child("Authors").PostAsync(perica);
        }
    }
}

